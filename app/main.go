package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"syreclabs.com/go/faker"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

type Persona struct {
	NombreUsuario string    `json:"nombre_usuario"`
	Nombre        string    `json:"nombre"`
	Apellido      string    `json:"apellido"`
	Movil         string    `json:"movil"`
	Fotos         []string  `json:"fotos"`
	Ciudad        string    `json:"ciudad"`
	Direccion     string    `json:"direccion"`
	Email         string    `json:"email"`
	Web           string    `json:"web"`
	Edad          time.Time `json:"edad"`
}

func main() {
	// Echo instance
	e := echo.New()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))
	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	u := e.Group("api")
	// Routes
	u.GET("/person", get)

	// Start server for debug
	// e.Logger.Fatal(e.Start(":8080"))
	// Start server for prod
	e.Logger.Fatal(e.Start(":80"))
}

// Handler
func get(c echo.Context) error {
	quantity := c.QueryParam("quantity")
	if quantity == "" {
		quantity = "1"
	}

	vueltas, err := strconv.Atoi(quantity)

	if err != nil {
		return c.JSON(400, "")
	}

	fmt.Println("vueltas", vueltas)
	if vueltas > 1000 {
		return c.JSON(400, "Quantity too high")
	}

	var personas []Persona

	for i := 0; i < vueltas; i++ {
		persona := Persona{}
		GenerarDatosPersona(&persona)
		personas = append(personas, persona)
	}

	return c.JSON(http.StatusOK, personas)
}

func GenerarDatosPersona(p *Persona) {
	p.NombreUsuario = faker.Internet().UserName()
	p.Nombre = faker.Name().FirstName()
	p.Apellido = faker.Name().LastName()
	p.Movil = faker.PhoneNumber().CellPhone()
	p.Edad = faker.Time().Birthday(16, 70)
	rand := rand.Intn(10-0) + 0

	for i := 0; i < rand; i++ {
		p.Fotos = append(p.Fotos, faker.Avatar().String())
	}

	p.Ciudad = faker.Address().City()
	p.Direccion = faker.Address().String()
	p.Email = faker.Internet().FreeEmail()
	p.Web = faker.Internet().DomainName()
}
